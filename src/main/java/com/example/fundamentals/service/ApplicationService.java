package com.example.fundamentals.service;

import java.util.List;

import com.example.fundamentals.entity.Application;

public interface ApplicationService {
	
	  List<Application> listApplications();
	    Application findApplication(long id);


}

package com.example.fundamentals.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.fundamentals.entity.Release;
import com.example.fundamentals.repository.ReleaseRepository;

public class ReleaseServiceImpl implements ReleaseService{
	
	@Autowired
	private ReleaseRepository releaseRepository;

	@Override
	public List<Release> listRelease() {
		
		return (List<Release>)releaseRepository.findAll();
	}

}

package com.example.fundamentals.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.fundamentals.entity.Release;

public interface ReleaseRepository extends CrudRepository<Release, Long>{

}

package com.example.fundamentals.exception;

public class ApplicationNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8328160809195175089L;
	
	public ApplicationNotFoundException(String exception) {
		super(exception);
	}

}

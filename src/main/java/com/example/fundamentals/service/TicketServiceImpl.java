package com.example.fundamentals.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.fundamentals.entity.Ticket;
import com.example.fundamentals.repository.TicketRepository;

@Service
public class TicketServiceImpl implements TicketService {
	@Autowired
	private TicketRepository ticketRepository;

	@Override
	public List<Ticket> listTicket() {
		
		return (List<Ticket>)ticketRepository.findAll();
	}

}

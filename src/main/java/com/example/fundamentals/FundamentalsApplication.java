package com.example.fundamentals;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.fundamentals.entity.Application;
import com.example.fundamentals.entity.Release;
import com.example.fundamentals.entity.Ticket;
import com.example.fundamentals.repository.ApplicationRepository;
import com.example.fundamentals.repository.ReleaseRepository;
import com.example.fundamentals.repository.TicketRepository;

@SpringBootApplication
public class FundamentalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundamentalsApplication.class, args);
		System.out.print("Hello");
	}
	
	
	
	  @Bean public CommandLineRunner demo (ApplicationRepository repository) {
	  return(args) -> { repository.save(new Application("TemperatureTracker",
	  "Nab.nath", "Application for tracking temperature." )); repository.save(new
	  Application("BugTracker", "Nab.nath", "Application for tracking bug." ));
	  repository.save(new Application("DataTracker", "Nab.nath",
	  "Application for tracking data." )); repository.save(new
	  Application("TimeTracker", "Nab.nath", "Application for tracking time." ));
	  }; }
	  
	  
	  @Bean public CommandLineRunner addDataRelease(ReleaseRepository release) {
	  return (args) -> { release.save(new Release(1,
	  "This is new release Trackzilla", "2020/4/29")); }; }
	  
	  @Bean public CommandLineRunner addDataTicket(TicketRepository ticket) {
	  return (args) -> { ticket.save(new Ticket("Error",
	  "Error in new release Trackzilla",null, null, "OPen")); }; }
	 
}

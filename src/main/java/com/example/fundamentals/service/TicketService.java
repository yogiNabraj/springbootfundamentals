package com.example.fundamentals.service;

import java.util.List;

import com.example.fundamentals.entity.Ticket;

public interface TicketService {
	List<Ticket> listTicket();

}

package com.example.fundamentals.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.fundamentals.entity.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Long>{

}

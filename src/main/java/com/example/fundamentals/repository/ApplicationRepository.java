package com.example.fundamentals.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.fundamentals.entity.Application;

public interface ApplicationRepository extends CrudRepository<Application, Long>{

}

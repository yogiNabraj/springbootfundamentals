package com.example.fundamentals.service;

import java.util.List;

import com.example.fundamentals.entity.Release;

public interface ReleaseService {
	List<Release> listRelease();

}
